
const swal = require('../Utilities/ModelsPlugin/sweetalert2.all')

const swalPlugin = {}

swalPlugin.install = function (Vue) {
    Vue.prototype.$swal = swal
}

Vue.use(swalPlugin)