const { Router } = require('express');
const updateJsonFile = require('update-json-file')
var cryptorjs = require('cryptorjs')
var myCryptor = new cryptorjs('KaioKenx100alafergatodo');
const path = require('path')
require('express-router-group')
router = Router();
var multer = require('multer')
const fs = require('fs')
const qs = require('query-string')

//Product files
const { Product } = require('../Controllers/productController');
const product = require('../Models/productModel');

//Product Category files
const { ProdCategory } = require('../Controllers/ProdCategoryController');
const category = require('../Models/productCategoryModel');


router.post('/uploadimg', (req, res) => {
    let storage = multer.diskStorage({
        destination:(req, file, cb) => {
            cb(null,path.join(__dirname,'../../public/img/products'))
        },
        filename: (req, file, cb) => {
            cb(null, Date.now() + path.extname(file.originalname) )
        }
    })
    let maxSize = 1000 * 1024
    // let upload = multer({ storage }).array('images',4)
    let upload = multer({ storage:storage,limits: { fileSize: maxSize } }).array('images',4)

    upload(req,res,function(err) { 
        // console.log(req.files);
        let id = req.body.idp.toString()
        // let idc = req.body.idc.toString()
        if(err) {
            if(err.code === 'LIMIT_FILE_SIZE')  
            { 
                res.json({status:"exceed"})
                product.deleteOne({ _id: id }, (err) => {
                    if (err) return handleError(err);
                    // else
                    //   console.log('delete exceed')  
                  });
                return res.end("exceed limit")

            }
            else    
            {
                res.json({status:"errorup"}) 
                product.deleteOne({ _id: id }, (err) => {
                    if (err) return handleError(err);
                    // else
                    // console.log('delete error upload')
                }); 
                return res.end("Error uploading file.");
            }

        }
        // let id = req.body.idp.toString()
        // let idc = req.body.idc.toString()
        let images = []
        let n = Math.floor(Math.random()*10)+1
        for (let index = 0; index < req.files.length; index++) {
            // let data_ = id+"_"+req.files[index].filename
            let data_ = id+n+Date.now()+path.extname(req.files[index].filename)
            let originsrc = path.join(__dirname,'../../public/img/products',req.files[index].filename)
            let newsrc = path.join(__dirname,'../../public/img/products',data_)
            try {
                let dirf = path.join(__dirname,'../../public/img/products') 
                let mk = path.join(dirf,'/'+id) 
                if (!fs.existsSync(mk)){
                    fs.mkdirSync(mk);
                    let dir = path.join(mk,data_)
                    fs.renameSync(originsrc,dir);
                }else{
                        let dir = path.join(mk,data_)
                        fs.renameSync(originsrc,dir);
                }
                // fs.renameSync(originsrc,newsrc);
            } catch (err) {
                console.error(err)
            }
            let dataimg = id+"/"+data_
            images.push(dataimg.toString())
            
        } 
        let data = product.findByIdAndUpdate(id,{ "$push": { "images": images } },
        { "new": true, "upsert": true },function (err, managerparent) {
        if (err) throw err;
        if('_id' in managerparent)
        {
            res.json({status:'success'}) 
        }
        else{
            res.json({status:'failed'})
            product.deleteOne({ _id: id }, (err) => {
                if (err) return handleError(err);
                else
                  console.log('delete error update pro')  
              });
        }
        }) 
    })
})
router.post('/store', async (req, res) => {
    let data_ = myCryptor.decode(req.body.data);    
    // let data_ = req.body.data
    let data = await Product.store(data_);
    if(typeof data === 'string' || !('_id' in data)) {
        res.json({status: 'failed', data})
    }
    else {

        res.json({status: 'success', data})
    }
})

router.post('/turnDown', async (req, res) => {
    let data_ = req.body
    let data = await product.findOne({_id: data_.product});
    data.status = data_.status
    let result = await data.save();
    if(!('_id' in result)) {
        res.json({status: 'failed', data: 'Something went wrong, try later'})
    }
    else
        res.json({status: 'success', data: result})
})

router.post('/getLastOnes', async (req, res) => {
    let data = await product.find().sort({created_at: 1})
    res.json({status: 'success', data})
})

router.post('/getAll', async (req, res) => {
    let data = await Product.getAll(req.body.onlyActive)
    // console.log(data[0])
    if(data.length > 0)
        res.json({status: 'success', data})
    else
        res.json({status: 'failed', data})
})

router.post('/postTempData', (req, res) => {
    res.json({data: 'some data'})
})
router.post('/valCodePro', async(req,res) => {
    let code = req.body.data
    // let codep = code.toUpperCase() 
    let codepro = await product.findOne({code:code})
    if(codepro)
        res.json({status:'success', data:myCryptor.encode(codepro.code)})
    else
       res.json({status:'not found'}) 
})
router.group('/category', (router) => {
    router.post('/store', async (req, res) => {
        let data_ = myCryptor.decode(req.body.data);    
        // let data_ = req.body.data
        let data = await ProdCategory.store(data_);
        if(typeof data === 'string' || !('_id' in data)) {
            res.json({status: 'failed', data})
        }
        else {
            res.json({status: 'success', data})
        }
    })

    router.post('/getAll', async (req, res) => {
        let data = await category.find({},{
            name: 1,
            description: 1,
            image: 1,
            subName: 1
        })
        res.json({status: 'success', data})
    })

    router.post('/allOfThis', async (req, res) => {
        let data = await product.find({category: req.body.category, status: true},{
            name: 1,
            description: 1,
            slogan: 1,
            prices: 1,
            stock: 1,
            images: 1
        })
        if(data.length > 0){
            res.json({status: 'success', data})
        }else{
            res.json({status: 'failed', data: 'No products to this category'})
        }
    })
    //----------------------------upload image-------------------------------------
    router.post('/catimg', (req, res) => {
        let storage = multer.diskStorage({
            destination:(req, file, cb) => {
                cb(null,path.join(__dirname,'../../public/img/categories'))
            },
            filename: (req, file, cb) => {
                cb(null, Date.now() + path.extname(file.originalname) )
            }
        })
        let maxSize = 1000 * 1024 
        let upload = multer({ storage:storage, limits: { fileSize: maxSize } }).single('imgcat')
        upload(req, res, (err) => { 
            // console.log(req.files);
            let form = req.body
            let id = form.idc.toString()
            if(err) {
                if(err.code === 'LIMIT_FILE_SIZE')  
                { 
                    res.json({status:"exceed"})
                    category.deleteOne({ _id: id }, (err) => {
                        if (err) return handleError(err);
                        else
                          console.log('delete exceed')  
                      });
                    return res.end("exceed limit")

                }
                else    
                {
                    res.json({status:"errorup"}) 
                    category.deleteOne({ _id: id }, (err) => {
                        if (err) return handleError(err);
                        else
                        console.log('delete error upload')
                    }); 
                    return res.end("Error uploading file.");
                }
            }
    
            // let id = req.body.idc.toString()
            let data_ = id+path.extname(req.file.filename)
            let originsrc = path.join(__dirname,'../../public/img/categories',req.file.filename)
            let newsrc = path.join(__dirname,'../../public/img/categories',data_)
            try {
                let dirf = path.join(__dirname,'../../public/img/categories') 
                let mk = path.join(dirf,'/'+id)
                if (!fs.existsSync(mk)){
                    fs.mkdirSync(mk);
                    let dir = path.join(mk,data_)
                    fs.renameSync(originsrc,dir);
                }else{
                    let dir = path.join(mk,newsrc)
                    fs.renameSync(originsrc,dir);
                }
                } catch (err) {
                console.error(err)
                }
            let image = data_.split('.')[0]+"/"+data_

            let data = category.findByIdAndUpdate(id, {image:image},
            { "new": true, "upsert": true },function (err, managerparent) {
            if (err) throw err;
            if('_id' in managerparent)
            {
                res.json({status:'success'}) 
            }
            else{
                res.json({status:'failed'})
                category.deleteOne({ _id: id }, (err) => {
                    if (err) return handleError(err);
                    else
                    console.log('delete error update')
                });
            }
            }) 
        })
    })
})
router.group('/update', router => {
    router.post('/stock', async(req,res) => {
        // console.log(req.body.data)
        let data = myCryptor.decode(req.body.data)
        let data_ = await Product.updateSingleStock(data)
        if(!('_id' in data_)) {
            res.json({status: 'failed', data: myCryptor.encode(data_)})
        }
        else {
            res.json({status: 'success', data: myCryptor.encode(data_)})
        }
    })
    router.post('/infoproduct',async(req,res) => {
        let data = myCryptor.decode(req.body.data)
        let data_ = product.findByIdAndUpdate(data.id, data.data,
            { "new": true, "upsert": true },function (err, managerparent) {
            if (err) throw err;
            if('_id' in managerparent)
            {
                res.json({status:'success',data:myCryptor.encode(managerparent)}) 
            }
            else{
                res.json({status:'failed'})
            }
            }) 
    }),
    router.post('/uploadimgs',async(req,res) => {
        let storage = multer.diskStorage({
            destination:(req, file, cb) => {
                cb(null,path.join(__dirname,'../../public/img/products'))
            },
            filename: (req, file, cb) => {
                cb(null, Date.now() + path.extname(file.originalname) )
            }
        })
        let maxSize = 1000 * 1024
        let upload = multer({ storage:storage,limits: { fileSize: maxSize } }).array('images',4)
        upload(req,res, async function(err) { 
            let id = req.body.idp.toString()
            // let idc = req.body.idc.toString()
            let del = req.body.del
            let imagesarray = req.body.temp
            // let temp = req.body.temp
            let temp = []

            if(typeof(imagesarray) == 'object')
           { 
               for (let index = 0; index < imagesarray.length; index++) {
                
                temp.push(imagesarray[index])

                }
            }
            else
            {
                temp.push(imagesarray)
            }
            // console.log('temp:',temp)            
            if(err) {
                if(err.code === 'LIMIT_FILE_SIZE')  
                { 
                    res.json({status:"exceed"})
                    return res.end("exceed limit")
    
                }
                else    
                {
                    res.json({status:"errorup"}) 
                    return res.end("Error uploading file.");
                }
    
            }
            // let images = []
            let n = Math.floor(Math.random()*10)+1
            for (let index = 0; index < req.files.length; index++) {
                // let data_ = id+"_"+req.files[index].filename
                let data_ = id+n+Date.now()+path.extname(req.files[index].filename)
                let originsrc = path.join(__dirname,'../../public/img/products',req.files[index].filename)
                let newsrc = path.join(__dirname,'../../public/img/products',data_)
                try {
                    let dirf = path.join(__dirname,'../../public/img/products') 
                    let mk = path.join(dirf,'/'+id) 
                    if (!fs.existsSync(mk)){
                        fs.mkdirSync(mk);
                        let dir = path.join(mk,data_)
                        fs.renameSync(originsrc,dir);
                    }else{
                            let dir = path.join(mk,data_)
                            fs.renameSync(originsrc,dir);
                    }
                    // fs.renameSync(originsrc,newsrc);
                } catch (err) {
                    console.error(err)
                }
                let dataimg = id+"/"+data_
                temp.push(dataimg.toString())  //antes mover de categorias si se cambio esta   
            }
            // DELETE IMGS
            if(del && typeof del == 'object' && del.length > 0)
            {
                // console.log(data.del)
                for (let index = 0; index < del.length; index++) {
                let dir =  path.join(__dirname,'../../public/img/products',del[index].name)
                fs.unlink(dir, function (err) {
                            if (err) throw err;
                    });  
                
                }
            }
            let dataup = await product.findOne({_id:id});
            dataup.images = temp
             let save = await dataup.save()
             if(!save || !('_id' in save))
             {
                 res.json({status:'failed',data:myCryptor.encode(save)}) 
             }
             else{
                 res.json({status:'success',data:myCryptor.encode(save)})
             }
        })
    })
    router.post('/updateimgs', async(req,res) => {
        let data = req.body
        // console.log(data.del)
        //DELETE IMGS
        if(data.del.length > 0)
        {
            for (let index = 0; index < data.del.length; index++) {
            let dir =  path.join(__dirname,'../../public/img/products',data.del[index].name)
            fs.unlink(dir, function (err) {
                        if (err) throw err;
                });  
            
            }
        }
    
        let datap = await product.findOne({_id:data.id});
        datap.images = data.temp
            let save = await datap.save()
            if(!save || !('_id' in save))
            {
                res.json({status:'failed',data:myCryptor.encode(save)}) 
            }
            else{
                res.json({status:'success',data:myCryptor.encode(save)})
            }

    })
})

module.exports = { router }