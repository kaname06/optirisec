const { Router } = require('express');
const updateJsonFile = require('update-json-file')
var cryptorjs = require('cryptorjs')
var myCryptor = new cryptorjs('KaioKenx100alafergatodo');
var bcrypt = require('bcryptjs');
const path = require('path')
const s = require('../../mailer')
const rs = require('randomstring')
require('express-router-group')

router = Router();
//Auth files
const { AuthEC } = require('../Controllers/authController');
const authec = require('../Models/authModel');

//Rec Auth Files
// const { Auth } = require('../../../../src/Controllers/authController');//audit this
const auth = require('../Models/authOPTModel');

//Worker Auth Files
const { Worker } = require('../Controllers/workerController');
const worker = require('../Models/workerModel');

const { Seller } = require('../Controllers/sellerController')
const seller = require('../Models/sellerModel')

const purchase = require('../Models/purchaseModel')

const { sendMailForgotPass } = require('../../mailer')

router.get('/login', (req, res) => {
    if (req.session.user && req.cookies.user_session_id) {
        res.redirect('/')
    }
    else{
        res.sendFile(path.join(__dirname,'../../public/login.html'));
    }
})

router.get('/register', (req, res) => 
{    
    res.sendFile(path.join(__dirname,'../../public/register.html'));
})

router.post('/getPersonByParams', async (req, res) => {
    let data_ = await authec.find(req.body.queryParams);
    let data = []
    if(data_ == null || data_.length <= 0){
        res.json({status: 'failed', data})
    }else {        
        res.json({status: 'success', data})
    }
})

router.post('/store', async (req, res) => {
    let data_ = myCryptor.decode(req.body.data);    
    // let data_ = req.body
    let data = await AuthEC.store(data_);
    if(typeof data === 'string' || !('_id' in data)) {
        res.json({status: 'failed', data})
    }
    else {
        res.json({status: 'success', data})
    }
})

router.post('/validate', async (req, res) => {
    let data_ = myCryptor.decode(req.body.data);
    // let data_ = req.body.data
    let dat = data_;
    let nick = dat.nickname
    let pass = dat.password
    let resp = ''
        let data = await authec.findOne({'accessData.userCode': nick}) || await auth.findOne({'accessData.userCode': nick}) || await worker.findOne({'accessData.nickname': nick})
        if(data) {
            if(data && data.validatePass(pass)){
                resp = 'Authenticated'
                
                let userType = 'public'
                if(data.sons)
                    userType = 'rec'
                if(data.companyRol)
                    userType = 'worker'
                let datitoss = {
                    name: data.name,
                    dni: data.dni,
                    _id: data._id,
                    mail: data.mail,
                    address: data.address || null,
                    avatar: data.accessData.avatar,
                    gender: data.gender,
                    phone: data.phone || null,
                    lastAccess: data.accessData.lastAccess,
                    userType
                }                    
                req.session.user = datitoss
                updateJsonFile('u23r20nl1n3.txt', (dat) => {
                    let adr = false
                    for (let index = 0; index < dat.length; index++) {
                        if(dat[index].dni == req.session.user.dni.dni)
                        {
                            adr = true
                        }   
                    }
                    if(!adr)
                        dat.push({dni: req.session.user.dni.dni})
                    return dat;
                })
                let lafec = new Date()
                lafec.setHours(lafec.getHours() - 5)
                data.accessData.lastAccess = lafec;
                await data.save();
            }
            else
                resp = 'Contraseña incorrecta'
        }
        else
            resp = 'Usuario no encontrado'

    var status = 'success'
    if(resp != 'Authenticated')
        status = 'failed'
    res.json({user: nick, response: resp, status: status, sess: req.session.user})
})

router.post('/sessdata', (req,res) => {
    if (req.session.user && req.cookies.user_session_id) {
        let temp = req.session.user;
        let data = [];
        for (const key in req.body.fields) {
            let a = req.body.fields[key];
            let ths = temp[a];
            data.push(ths)
        }
        let resp = myCryptor.encode(data)
        res.json({status: 'success', data: resp});
    }
    else {
        res.json({status:'failed'})
    }
})

router.group('/worker', (router) => {
    router.post('/store', async (req, res) => {
        // let data_ = myCryptor.decode(req.body.data);    
        let data_ = req.body
        let data = await Worker.store(data_);
        if(typeof data === 'string' || !('_id' in data)) {
            res.json({status: 'failed', data})
        }
        else {
            res.json({status: 'success', data})
        }
    })
})

router.group('/seller', (router) =>
{
    router.post('/store', async (req, res) =>
    {
        let data_ = req.body
        let data = await Seller.store(data_);
        if(typeof data === 'String' || !('_id' in data))
        {
            res.json({status: 'failed', data})
        } else {
            res.json({status: 'success'})
        }
    })

    router.post('/unique', async (req, res) =>
    {        
        let uniq = await seller.findOne(req.body.query) || await worker.findOne(req.body.query) || await worker.findOne(req.body.query) 
        // console.log(uniq);
        if(typeof uniq === 'String' || !uniq)
        {
            res.json({status: false, message: 'data not found'})
        } else {
            res.json({status: true, message: 'this value exists in the DB'})
        }
    })

    router.post('/update/:dead?', async (req, res) =>
    {
        if(req.params.dead == 'kill-him')
        {            
            let data_ = await seller.findByIdAndUpdate(req.body.id, {'status': !req.body.status}) 
            let saving = await data_.save()
            if(!saving || !saving._id)
                return res.json({
                    success: false,
                    info: "An error ocurred while saving data"
                })
            return res.json({
                success: true,
                info: "Seller Deleted successfully"
            })
        }
        let data_ = await seller.findByIdAndUpdate(req.body.id, req.body.data) 
        let saving = await data_.save()
        if(!saving || !saving._id)
            return res.json(
            {
                success: false,
                info: "An error ocurred while saving data"
            })
        return res.json(
        {
            success: true,
            info: "Seller updated successfully"
        })
    })

    router.post('/getAll/:f?', async (req, res) => 
    {
        let data = ''
        if(req.params.f == 'front')
            data = await seller.find({status: true})
        else 
            data = await seller.find()
        
        let pur = await purchase.find({purchaseStatus: 'Approved'}).sort({payDate:'desc'})
        let save = []
        let data__ = 
        {
            name: 
            {
                first: '',
                last: ''
            },
            dni: 
            {
                dniType: '',
                dni: ''
            },
            created_at: '',
            status: '',
            phone: '',
            mail: '',
            code: '',
            purchases: []
        }
        if(data.length > 0)
        {                              
            for (const key in data) 
            {                
                data__.sellerName = data[key].name.first+' '+data[key].name.last
                data__._id = data[key]._id
                data__.name = data[key].name
                data__.dni = data[key].dni
                data__.phone = data[key].phone
                data__.mail = data[key].mail
                data__.created_at = data[key].created_at
                data__.code = data[key].code
                data__.status = data[key].status

                // console.log(!pur, pur, pur.length, ('_id' in pur));
                if(pur.length > 0 && ('_id' in pur))
                {
                    for (const keyy in pur) 
                    {                    
                        if(data[key]._id.toString() == pur[keyy].seller.toString())
                        {                        
                            data__.purchases.push({orders: pur[keyy].orderDetails, date: pur[keyy].payDate, amount: pur[keyy].totalAmountToPay, _id: pur[keyy]._id})                        
                        }
                    }
                }

                save.push(data__)   
                data__ = 
                {
                    name: 
                    {
                        first: '',
                        last: ''
                    },
                    dni: 
                    {
                        dniType: '',
                        dni: ''
                    },
                    created_at: '',
                    status: '',
                    phone: '',
                    mail: '',
                    code: '',
                    purchases: []
                }           
            }
            // console.log(data);
            let data_ = myCryptor.encode(save);
            res.json({status: 'success', data: data_})
        } else {
            res.json({status: 'success', data: []})
        }
    })

    router.post('/info', async (req, res) =>
    {
        let data = await seller.findById(req.body.id)
        let data_ = myCryptor.encode(data);
        if(typeof data === 'String' || !('_id' in data))
        {
            res.json({status: 'failed', data})
        } else {
            res.json({status: 'success', data: data_})
        }
    })
})

router.post('/send-mail', async (req, res) =>
{
    let data = await auth.findById(req.body.data.Rid) 
    if(data.validatePass(myCryptor.decode(req.body.data.Rpass))){
        let a = await s.sendR(req.body.data)
        if(a.response.includes('OK'))
        {        
            return res.json({status: 'success', mes: 'Reclamo enviado exitosamente'})        
        } else {        
            return res.json({status: 'success', mes: 'Error al enviar el reclamo'})          
        }
    }else{
        return res.json({status: 'pass', mes: 'Contraseña no coincide'})
    }

})

router.post('/send/mail/restore', async (req, res) => {
    const { nick, mail} = myCryptor.decode(req.body.data)
    if(!nick || !mail)
        return res.json({
            success: false,
            info: "Invalid query structure"
        })
    let shortvali = await auth.findOne({'accessData.userCode': nick, mail}) || await authec.findOne({'accessData.userCode': nick, mail})
    if(!shortvali || !shortvali._id)
        return res.json({
            success: false,
            info: "User not found"
        })
    let result = await sendMailForgotPass(nick, mail, shortvali.accessData.token)
    return res.json(result)
})

router.post('/reset/ur/pass', async (req, res) => {
    const { nickname, token, password } = myCryptor.decode(req.body.data)
    if(!nickname || typeof nickname != 'string' || !token || typeof token != 'string')
        return res.json({
            success: false,
            info: "Invalid query structure"
        })
    let eldato = await auth.findOne({'accessData.userCode': nickname, 'accessData.token':token}) || await authec.findOne({'accessData.userCode': nickname, 'accessData.token':token})
    if(!eldato || !eldato._id)
        return res.json({
            success: false,
            info: "Invalid data provided"
        })
    eldato.accessData.password = await auth.encryptPass(password);
    eldato.accessData.token = rs.generate({
        length: 12,
        charset: 'alphabetic'
    })
    let saving = await eldato.save()
    if(!saving || !saving._id)
        return res.json({
            success: false,
            info: "An error ocurred while saving data"
        })
    return res.json({
        success: true,
        info: "Password updated successfully"
    })
})

router.get('/reset/password/:nick/:token',(req,res) => {
    res.sendFile(path.join(process.cwd(), '/public/reset.html'));
})

module.exports = {router}