const { Router } = require('express');
const updateJsonFile = require('update-json-file')
var cryptorjs = require('cryptorjs')
var myCryptor = new cryptorjs('KaioKenx100alafergatodo');
const path = require('path')
const qs = require('query-string')
require('express-router-group')

router = Router();
//Purchase files
const { Purchase } = require('../Controllers/purchaseController');
const purchase = require('../Models/purchaseModel');

//Product files
const { Product } = require('../Controllers/productController')
const product = require('../Models/productModel')

//Auth Models
const authOPT = require('../Models/authOPTModel');
const authEC = require('../Models/authModel');

router.post('/store', async (req, res) => {
    if(req.session.user && req.cookies.user_session_id && req.session.user.userTpe != 'worker') {
        // let data_ = myCryptor.decode(req.body.data);    
        let data_ = req.body
        data_.buyer = req.session.user._id
        let query = await purchase.find({buyer: req.session.user._id, purchaseStatus: 'inBag'})
        if(query.length <= 0){
            let data = await Purchase.store(data_);
            if(typeof data === 'string' || !('_id' in data)) {
                res.json({status: 'failed', data})
            }
            else {
                res.json({status: 'success', data})
            }
        }
        else {
            let data = await Purchase.updateBag(data_);
            if(data != 'success') {
                res.json({status: 'failed', data})
            }
            else {
                res.json({status: 'success', data})
            }
        }
    }
})

router.post('/validateBagToSell', async (req, res) => {
    if(req.session.user && req.cookies.user_session_id && req.session.user.userTpe != 'worker') {
        let errors = []
        let data = await purchase.findOne({buyer: req.session.user._id})
        for (let index = 0; index < data.orderDetails.length; index++) {
            let found = false
            let elpro = await product.findById(data.orderDetails[index].product);
            if(!elpro.status)
                errors.push({product: data.orderDetails[index].product, reasons: ['product inactive']})
            if(elpro.stock < data.orderDetails[index].quantity) {
                for (let ind = 0; ind < errors.length; ind++) {
                    if(errors.product == data.orderDetails[index].product) {
                        found = true
                        errors.reasons.push("quantity exceed disponible stock")
                        break;
                    }
                }
                if(!found)
                    errors.push({product: data.orderDetails[index].product, reasons: ["quantity exceed disponible stock"]})
            }
        }
        res.json({status: 'success', data: errors})
    }
    else
        res.json({status: 'failed', data: 'access denied'})
})

router.post('/getMyCart', async (req, res) => {
    if(req.session.user && req.cookies.user_session_id && req.session.user.userTpe != 'worker') {
        let data = await purchase.findOne({buyer: req.session.user._id, purchaseStatus: 'inBag'});
        if(data) {
            if(!('_id' in data)){
                res.json({status: 'failed', data: 'This user do not has products in the shopping cart'})
            }
            else {
                for (let index = 0; index < data.orderDetails.length; index++) {
                    let pr = await product.findById(data.orderDetails[index].product)
                    if(!pr.status) {
                        data.totalAmount = data.totalAmount - (pr.prices[data.purchaseType] * data.orderDetails[index].quantity)
                        if(data.purchaseType == 'rec')
                            data.totalPoints = data.totalAmount/5000
                        data.orderDetails.splice(index, 1)
                    }
                }
                let resi = await data.save()
                if(!('_id' in resi)) {
                    res.json({status: 'failed', data: 'an error was ocurred, try later'})
                }
                else {
                    let returndata = {
                        totalAmount: data.totalAmountToPay,
                        totalPoints: data.totalPoints,
                        orderDetails: data.orderDetails,
                        paymentRef: data.paymentRef
                    }
                    res.json({status: 'success', data: returndata})
                }
            }
        }
        else
            res.json({status: 'failed', data: 'This user do not has products in the shopping cart'})
    }
    else
        res.json({status: 'failed', data: 'access denied'})
})

router.post('/quitToCart', async (req, res) => {
    if(req.session.user && req.cookies.user_session_id && req.session.user.userTpe != 'worker') {
        let data_ = req.body
        data_.buyer = req.session.user._id
        let data = await Purchase.updateBag(data_, 'discard');
        if(data == 'failed') {
            res.json({status: 'failed', data})
        }
        else {
            res.json({status: 'success', data})
        }
    }
    else
        res.json({status: 'failed', data: 'access denied'})
})

router.post('/getMyBuys', async (req, res) => {
    if(req.session.user && req.cookies.user_session_id && req.session.user.userType != 'worker') {
        let data = await purchase.find({buyer: req.session.user._id, purchaseStatus: { $ne: 'inBag'}});
        res.json({status: 'success', data: myCryptor.encode(data)})
    }
    else
        res.json({status: 'failed', data: 'access denied'})
})
router.post('/allOrders', async (req, res) => {
    if(req.session.user && req.cookies.user_session_id && req.session.user.userType == 'worker') {
        let data = await purchase.find({purchaseStatus:'Approved'}).sort({payDate:'desc'})
        // console.log(data);
        let data_ = await Purchase.structureDataOrders(data)
        if(data_.length > 0)
            res.json({status: 'success', data: myCryptor.encode(data_)})
        else
            res.json({status: 'error', data: myCryptor.encode(data_)})
    }
    else
        res.json({status: 'failed', data: 'access denied'})
})
router.post('/upStatus', async (req, res) => {
    if(req.session.user && req.cookies.user_session_id && req.session.user.userType == 'worker') {
        let data = myCryptor.decode(req.body.data)
        let data_ = await Purchase.changeStatusOrder(data)
        if(!('_id' in data_))
            res.json({status: 'error', data: myCryptor.encode(data_)})
        else
            res.json({status: 'success', data: myCryptor.encode(data_)})
    }
    else
        res.json({status: 'failed', data: 'access denied'})
})

router.post('/modifyUnits', async (req, res) => {
    let entry = req.body.orderDetails //this object must be like this {product: a7f89sd7g8f768df76d, units: 2}
    let sell = req.body.seller
    // console.log(sell);
    // return res.json(entry)
    let resi = await Purchase.updateBag({buyer: req.session.user._id, od: entry, sell}, req.body.action)
    if(resi != 'success')
        res.json({status: 'failed', data:resi})
    else
        res.json({status: 'success', data:resi})
})

router.group('/callback', (router) => {
    router.post('/confirmation', async (req, res) => {
        let cont = ''
        let post = null
        req.on('data', function(chunk) {
            cont += chunk
        });
        req.on('end', async function() {
            post = qs.parse(cont)
            let prc = await purchase.findOne({paymentRef: post.reference_sale, purchaseStatus: {$ne: "Approved"}})
            if(prc && ('_id' in prc)) {
                if(post.state_pol == 4){
                    console.log('purchase approved by reference', post.reference_pol)
                    prc.purchaseStatus = "Approved"
                    prc.internalStatus.paymentReferenceCode = post.reference_pol
                    prc.internalStatus.status = "ready"
                    prc.payDate = new Date();
                    let prcd = await prc.save()
                    while (!('_id' in prcd)) {
                        prcd = await prc.save()
                    }
                    await Product.updateStock(prcd.orderDetails)
                }
                else {
                    if(post.state_pol == 6){
                        prc.purchaseStatus = "Rejected"
                        let prcd = await prc.save()
                        while (!('_id' in prcd)) {
                            prcd = await prc.save()
                        }
                    }
                }
            }
            res.status(200)
        });
    })

    router.get('/response', (req, res) => {
        console.log('llegó')
        res.redirect(url.format({
            pathname:"/",
            query:req.query
            })
        )
    })
})

let getProductionOrders = async () => {
    let todayF = new Date
    todayF.setHours(0)
    todayF.setMinutes(0)
    todayF.setSeconds(0)

    let todayL = new Date
    todayL.setHours(23)
    todayL.setMinutes(59)
    todayL.setSeconds(59)
    let data = await purchase.find({
        payDate: {
            $gte: todayF,
            $lte: todayL
        },
        purchaseStatus: 'Approved',
        'internalStatus.status': 'waiting'
    })
    let structuredData = []
    for (let index = 0; index < data.length; index++) {
        let buyer = await authOPT.findById(data[index].buyer) || await authEC.findById(data[index].buyer)
        if(buyer) {
            let bonesito = {
                buyer: buyer.name.first + ' ' + buyer.name.last,
                address: buyer.address.addInfo + ', ' + buyer.address.country + ' - ' +buyer.address.city,
                mail: buyer.mail || 'No Posee',
                dni: buyer.dni.dni,
                phone: buyer.phone,
                payReference: data[index].internalStatus.paymentReferenceCode,
                buyReference : data[index].paymentRef,
                orderDetails: []
            }

            for (let ind = 0; ind < data[index].orderDetails.length; ind++) {
                let elpro = await product.findById(data[index].orderDetails[ind].product)
                if(elpro) {
                    let otherbonesito = {
                        code: elpro.code,
                        product: elpro.name,
                        quantity: data[index].orderDetails[ind].quantity
                    }

                    bonesito.orderDetails.push(otherbonesito)
                }
            }

            structuredData.push(bonesito)
        }
    }

    return structuredData;
}

module.exports = {router, getProductionOrders}