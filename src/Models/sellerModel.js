const { Schema, model } = require('mongoose')
var uniqueKeygen = require('unique-keygen');


const Seller = new Schema(
{
    name: 
    {
        first: 
        {
            type: String,
            required: true
        },
        last: 
        {
            type: String,
            required: true
        }
    },
    dni: 
    {
        dniType: 
        {
            type: String,
            required: true
        },
        dni: 
        {
            type: String,
            required: true
        }
    },
    created_at: 
    {
        type: Date,
        default: Date.now
    },
    status:
    {
        type: Boolean,
        default: true
    },
    phone: 
    {
        type: Number,
        required: true
    },
    mail:
    {
        type: String,
        required: true
    },
    code:
    {
        type: String,
        default: function()
        {
            return `${uniqueKeygen(3)}-${uniqueKeygen(3)}-${uniqueKeygen(3)}`
        }
    }
});

module.exports = model('Seller', Seller);