const {Schema, model, Types} = require('mongoose');

const Purchase = new Schema({
    buyer: {
        type: Schema.Types.ObjectId,
        required: true
    },
    seller:
    {
        type: Schema.Types.ObjectId,        
    },
    orderDetails: [{
        product: {
            type: Schema.Types.ObjectId,
            required: true
        },
        quantity: {
            type: Number,
            required: true
        }
    }],
    paymentRef: {
        type: String,
        default: function() {
            return "P"+this.purchaseType.substring(0,1).toUpperCase()+Types.ObjectId()
        }
    },
    purchaseType: {
        type: String,
        default: 'public',
        enum: ['public', 'rec']
    },
    totalAmountToPay: {
        type: Number,
        default: function() {
            if(this.purchaseType == 'rec') {
                if(this.totalAmount >= 1000000) {
                    let restantAmount = this.totalAmount - 1000000
                    return (restantAmount + 899000)
                }
                else
                    return this.totalAmount
            }
            else
                return this.totalAmount
        }
    },
    totalAmount: {
        type: Number,
        required: true
    },
    totalPoints: {
        type: Number,
        default: function() {
            if(this.purchaseType == 'rec')
                return Math.floor(this.totalAmount/5000)
            else
                return 0
        }
    },
    pointsUsed: {
        type: Boolean,
        default: false
    },
    purchaseStatus: {
        type: String,
        default: 'inBag',
        enum: ['inBag', 'Pending', 'Approved', 'Rejected']
    },
    internalStatus: {
        status: {
            type: String,
            default: 'waiting',
            enum: ['waiting', 'ready', 'delivered', 'received']
        },
        paymentReferenceCode: {
            type: String,
            default: null
        },
        deliveryDate: {
            type: Date,
            default: null
        },
        receivedDate: {
            type: Date,
            default: null
        },
        receiverName: {
            type: String,
            default: null
        }
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    payDate: {
        type: Date,
        default: null
    }
});

module.exports = model('purchase', Purchase);