const { Schema, model } = require('mongoose');
const bcrypt = require('bcryptjs');

const Worker = new Schema({
    name: {
        first: {
            type: String,
            required: true
        },
        last: {
            type: String,
            required: true
        }
    },
    dni: {
        dniType: {
            type: String,
            required: true
        },
        dni: {
            type: String,
            required: true
        }
    },
    companyRol: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        enum: ['M', 'F', 'O']
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    accessData: {
        nickname: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        lastAccess: {
            type: Date,
            default: Date.now
        },
        status: {
            type: Boolean,
            default: true
        }
    }
})

Worker.statics.encryptPass = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
}

Worker.methods.validatePass = function (password) {
    return bcrypt.compareSync(password, this.accessData.password)
} 

module.exports = model('Worker', Worker)