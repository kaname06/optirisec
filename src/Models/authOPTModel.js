const { Schema, model } = require('mongoose');
const bcrypt = require('bcryptjs');

let ColombianDate = new Date()
ColombianDate.setHours(ColombianDate.getHours() - 5)

const Auth = new Schema({
    name: {
        first: {
            type: String,
            required: true
        },
        last: {
            type: String,
            required: true
        }
    },
    gender: {
        type: String,
        enum: ['M', 'F','O']
    },
    address: {
        country: {
            type: String,
            required: true
        },
        city: {
            type: String,
            required: true
        },
        addInfo: {
            type: String
        }
    },
    dni: {
        dniType: {
            type: String,
            required: true
        },
        dni: {
            type: String,
            required: true
        }
    },
    sonsNumber: {
        type: Number,
        default: 0
    },
    sons: {
        left: {
            type: Schema.Types.ObjectId,
            default: null
        },
        right: {
            type: Schema.Types.ObjectId,
            default: null
        }
    },
    credits: {
        own: {
            type: String,
            default: 0
        },
        released: {
            type: String,
            default: 0
        }
    },
    mail: {
        type: String,
        default: null
    },
    phone: {
        type: String,
        default: null
    },
    bankAccount: {
        account: {
            type: String,
            default: null
        },
        bank: {
            type: String,
            default: null
        }
    },
    pattern: {
        type: Schema.Types.ObjectId,
        default: null
    },
    realPattern: {
        type: Schema.Types.ObjectId,
        default: null
    },
    created_at: {
        type: Date,
        default: ColombianDate
    },
    accessData: {
        userCode: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        userType: {
            type: String,
            required: true
        },
        status: {
            type: Boolean,
            default: false
        },
        live: {
            type: String,
            default: 'Live',
            enum: ['Live','Dead','Dying']
        },
        clasification: {
            type: Schema.Types.ObjectId
        },
        avatar: {
            type: String,
            default: '/src/images/avatar1.png'
        },
        token: {
            type: String,
            required: true
        },
        lastAccess: {
            type: Date,
            default: ColombianDate
        }
    }
},{ collection : 'auths' })

Auth.statics.encryptPass = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
}

Auth.methods.validatePass = function (password) {
    return bcrypt.compareSync(password, this.accessData.password)
} 

module.exports = model('auth', Auth)