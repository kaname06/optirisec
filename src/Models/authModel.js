const { Schema, model } = require('mongoose');
const bcrypt = require('bcryptjs');
const rs = require('randomstring')

let ColombianDate = new Date()
ColombianDate.setHours(ColombianDate.getHours() - 5)

const AuthEC = new Schema({
    name: {
        first : {
            type: String,
            required: true
        },
        last: {
            type: String,
            requried: true
        }
    },
    dni: {
        dniType: {
            type: String,
            required: true
        },
        dni: {
            type: String,
            required: true
        }
    },
    mail: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        default: null
    },
    gender: {
        type: String,
        required: true
    },
    address: {
        country: {
            type: String,
            required: true
        },
        city: {
            type: String,
            required: true
        },
        addInfo: {
            type: String,
            default: null
        }
    },
    created_at: {
        type: Date,
        default: ColombianDate
    },
    accessData: {
        userCode: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        avatar: {
            type: String,
            default: '/src/images/avatar1.png'
        },
        lastAccess: {
            type: Date,
            default: ColombianDate
        },
        token: {
            type: String,
            default: function() {
                return rs.generate({
                    length: 12,
                    charset: 'alphabetic'
                })
            }
        }
    }
});

AuthEC.statics.encryptPass = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
}

AuthEC.methods.validatePass = function (password) {
    return bcrypt.compareSync(password, this.accessData.password)
} 

module.exports = model('ecAuth', AuthEC)