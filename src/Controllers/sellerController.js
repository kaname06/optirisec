const seller = require('../Models/sellerModel')

class table
{
    async store(request)
    {
        let data = new seller(request)
        var error = data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return status;
        } else{
            let status = await data.save();
            if(!('_id' in status)) {
                return 'Fatal Error, unable to store Seller, try later'
            }else {
                return status
            }            
        }
    }
}

let Seller = new table();
module.exports = {Seller};