const auth = require ('../Models/authModel')

class table 
{
    async store(request)
    {
        request.accessData.password = auth.encryptPass(request.accessData.password)
        let data = new auth(request)
        var error = data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return status;
        } else{
            let status = await data.save();
            if(!('_id' in status)) {
                return 'Fatal Error, unable to store person, try later'
            }else {
                return status
            }
            // return status;
        }
    }
}

let AuthEC = new table();
module.exports= {AuthEC};