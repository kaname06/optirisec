const product = require('../Models/productModel');
const category = require('../Models/productCategoryModel')

class table 
{
    async store(request)
    {
        let data = new product(request)
        var error = data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return status;
        } else{
            let status = await data.save();
            if(!('_id' in status)) {
                return 'Fatal Error, unable to store product, try later'
            }else {
                return status
            }
            // return status;
        }
    }

    async updateStock(products, method = 'rest') {
        for (let index = 0; index < products.length; index++) {
            if(products[index].product && products[index].quantity) {
                if(method == 'rest')
                    products[index].quantity = products[index].quantity * -1

                let data = await product.findOne({_id: products[index].product})
                data.stock = data.stock + (products[index].quantity)
                await data.save()
            }
        }
    }
    async updateSingleStock(info) {
        let stock = info.stock
        let idp = info.id

        let dstock = await product.findById(idp);
        dstock.stock = stock;
        let result = await dstock.save();
        return result;
    }
    async updateProduct(info) {
        // console.log(info.data)
        let pro = info.data
        let idpro = info.id
        let dpro = await product.findById(idpro);
        dpro = pro
        let result = await dpro.save();
        return result;
    }
    async getAll(onlyActive) {
        let data;
        if(onlyActive)
            data = await product.find({status: true, stock: {$gt: 0}}).sort({created_at: 1})
        else
            data = await product.find().sort({created_at: 1})
        let newData = []
        for (const key in data) {
            let catName = await category.findOne({_id: data[key].category}, {_id: 1, name: 1})
            let bone = {
                _id: data[key]._id,
                idc: catName._id,
                name: data[key].name,
                code: data[key].code,
                sanitaryReg: data[key].sanitaryReg,
                description: data[key].description,
                slogan: data[key].slogan,
                prices: data[key].prices,
                stock: data[key].stock,
                status: data[key].status,
                images: data[key].images,
                category: catName.name
            }
            newData.push(bone)
        }
        return newData
    }
}

let Product = new table();
module.exports= {Product};