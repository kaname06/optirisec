const category = require('../Models/productCategoryModel');

class table 
{
    async store(request)
    {
        let data = new category(request)
        var error = data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return status;
        } else{
            let status = await data.save();
            if(!('_id' in status)) {
                return 'Fatal Error, unable to store product category, try later'
            }else {
                return status
            }
            // return status;
        }
    }
}

let ProdCategory = new table();
module.exports= {ProdCategory};