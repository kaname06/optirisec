const worker = require('../Models/workerModel');

class table 
{
    async store(request)
    {
        request.accessData.password = worker.encryptPass(request.accessData.password)
        request.accessData.nickname = request.accessData.nickname.toLowerCase()
        let data = new worker(request)
        var error = data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return status;
        } else{
            let status = await data.save();
            if(!('_id' in status)) {
                return 'Fatal Error, unable to store person, try later'
            }else {
                return status
            }
            // return status;
        }
    }
}

let Worker = new table();
module.exports= {Worker};