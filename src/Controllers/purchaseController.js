const fs = require('fs')
const path = require('path')
const purchase = require('../Models/purchaseModel');
const { Product } = require('./productController')
const product = require('../Models/productModel')
const auth = require('../Models/authModel')
const authec = require('../Models/authOPTModel')
const seller = require('../Models/sellerModel')

const discounts = JSON.parse(fs.readFileSync(path.join(__dirname,'../Utilities/keys.json'))).discounts

class table 
{
    async store(request)
    {
        let data = new purchase(request)
        var error = data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return status;
        } else{
            let status = await data.save();
            if(!('_id' in status)) {
                return 'Fatal Error, unable to store purchase, try later'
            }else {
                return status
            }
            // return status;
        }
    }

    async deleteMyBag(person) {
        let data = await purchase.findOne({buyer: person, purchaseStatus: 'inBag'});
        let bag = await purchase.findByIdAndRemove(data._id)
    }

    defineAmountToPay(amount, type) {
        if(discounts["1kkto899k"]) {
            if(type == 'rec') {
                if(amount >= 1000000) {
                    let restantAmount = amount - 1000000
                    return (restantAmount + 899000)
                }
                else
                    return amount
            }
            else
                return amount
        } //here must be the future discounts placed at keys.json file
        else
            return amount
    }

    async updateBag(request, action = 'add'){
        let data = await purchase.findOne({buyer: request.buyer, purchaseStatus: 'inBag'});
        if(data) {            
            if(action == 'add') {
                let productEl = await product.findOne({_id: request.orderDetails[0].product})
                // console.log(productEl.status)
                if(!productEl.status)
                    return 'failed by inactive product'
                if(productEl.stock <= 0)
                    return 'failed by sold out'
                let order = {
                    product: request.orderDetails[0].product,
                    quantity: parseInt(request.orderDetails[0].quantity)
                }
                let exist = false
                let pindex = 0
                let prods = data.orderDetails;
                for (let index = 0; index < prods.length; index++) {
                    if(prods[index].product == order.product){
                        exist = true
                        pindex = index
                        break;
                    }
                }
                if(exist){
                    if((data.orderDetails[pindex].quantity + order.quantity) > productEl.stock)
                        return 'failed by stock limit exceed'
                    data.orderDetails[pindex].quantity = data.orderDetails[pindex].quantity + order.quantity;
                }
                else{
                    if(order.quantity > productEl.stock)
                        return 'failed by stock limit exceed'
                    data.orderDetails.push(order)
                }
                
                data.totalAmount = data.totalAmount + (productEl.prices['public'] * request.orderDetails[0].quantity)
                data.totalAmountToPay = this.defineAmountToPay(data.totalAmount, 'public')
                if(data.purchaseType == 'rec')
                    data.totalPoints = Math.floor((data.totalAmount)/5000)
                let res = await data.save()
                if(!('_id' in res))
                    return "failed"
                else
                    return "success"
            }
            if(action == 'discard') {
                let productEl = await product.findOne({_id: request.orderDetails[0].product})
                let exist = false
                let pindex = 0
                let prods = data.orderDetails;
                for (let index = 0; index < prods.length; index++) {
                    if(prods[index].product == request.orderDetails[0].product){
                        exist = true
                        pindex = index
                        break;
                    }
                }
                if(exist){
                    data.totalAmount = data.totalAmount - (productEl.prices['public'] * data.orderDetails[pindex].quantity)
                    data.totalAmountToPay = this.defineAmountToPay(data.totalAmount, 'public')
                    if(data.purchaseType == 'rec')
                        data.totalPoints = Math.floor((data.totalAmount)/5000)
                    data.orderDetails.splice(pindex, 1)
                }
                else{
                    return 'this product was not in the bag'
                }
                let res = await data.save()
                if(!('_id' in res))
                    return "failed"
                else
                    return "success"
            }
            if(action == 'modifyUnits') {
                let r = seller.findById(request.sell.id)
                if(r.status)
                {
                    if(request.sell.status)
                    {
                        data.seller = request.sell.id
                    }
                }                
                let counter = 0
                const target = request.od.length
                data.totalAmount = 0
                for (let indi = 0; indi < request.od.length; indi++) {
                    let productEl = await product.findOne({_id: request.od[indi].product})
                    let exist = false
                    let pindex = 0
                    let prods = data.orderDetails;
                    for (let index = 0; index < prods.length; index++) {
                        if(prods[index].product == request.od[indi].product){
                            exist = true
                            pindex = index
                            break;
                        }
                    }
                    if(exist){
                        if((request.od[indi].units) > productEl.stock)
                            break;
                        data.totalAmount = data.totalAmount + (productEl.prices['public'] * request.od[indi].units)
                        data.totalAmountToPay = this.defineAmountToPay(data.totalAmount, 'public')
                        if(data.purchaseType == 'rec')
                            data.totalPoints = Math.floor((data.totalAmount)/5000)
                        data.orderDetails[pindex].quantity = request.od[indi].units
                    }
                    else{
                        break;
                    }
                    let res = await data.save()
                    if(!('_id' in res))
                        break;
                    else
                        counter++;
                }
                if (counter == target) {
                    return 'success'
                }
                else
                    return 'failed'
            }
            // if(action == 'quitunits') {
            //     let productEl = await product.findOne({_id: request.product})
            //     let exist = false
            //     let pindex = 0
            //     let prods = data.orderDetails;
            //     for (let index = 0; index < prods.length; index++) {
            //         if(prods[index].product == request.product){
            //             exist = true
            //             pindex = index
            //             break;
            //         }
            //     }
            //     if(exist){
            //         data.totalAmount = data.totalAmount - (productEl.prices[data.purchaseType] * request.units)
            //         data.totalAmountToPay = this.defineAmountToPay(data.totalAmount, data.purchaseType)
            //         if(data.purchaseType == 'rec')
            //             data.totalPoints = Math.floor((data.totalAmount)/5000)
            //         data.orderDetails[pindex].quantity = data.orderDetails[pindex].quantity - request.units
            //     }
            //     else{
            //         return 'this product was not in the bag'
            //     }
            //     let res = await data.save()
            //     if(!('_id' in res))
            //         return "failed"
            //     else
            //         return "success"
            // }
        }
    }
    async structureDataOrders(data){
        let infoOrders = []
        let dataOrders = {
           _id : '',
           buyer:{
            name:'',
            address:'',
            phone:'',
            mail:'',
            dni:{
                dni:'',
                dniType:''
            },
            userType:''
           },
           orderDetails:[],
           paymentRef:'',
           paymentRefCode:'',
           seller: 
           {
                name: '',
                id: '',
                code: ''
           },
           purchaseType:'',
           totalAmount:'',
           purchaseStatus:'',
           internalStatus:{
               status:'',
               deliveryDate:null,
               receivedDate:null,
               receiverName:'',
           },
           payDate:null
        }
        for (let index = 0; index < data.length; index++) {
           dataOrders._id = data[index]._id
           dataOrders.paymentRef = data[index].paymentRef
           let sel = await seller.findOne({_id: data[index].seller})            
           if(sel && ('_id' in sel))
           {
                let na = `${sel.name.first} ${sel.name.last}`;           
                dataOrders.seller.id = data[index].seller
                dataOrders.seller.name = na
                dataOrders.seller.code = seller.code
           }
        //    for (const key in sel) 
        //    {
        //         if(sel[key]._id == data[index].seller)
        //    }
           dataOrders.payDate = data[index].payDate
           dataOrders.totalAmount = data[index].totalAmount
           dataOrders.purchaseType = data[index].purchaseType
           dataOrders.purchaseStatus = data[index].purchaseStatus
           dataOrders.internalStatus.status = data[index].internalStatus.status
           dataOrders.internalStatus.deliveryDate = data[index].internalStatus.deliveryDate
           dataOrders.internalStatus.receivedDate = data[index].internalStatus.receivedDate
           dataOrders.internalStatus.receiverName = data[index].internalStatus.receiverName
           dataOrders.paymentRefCode = data[index].internalStatus.paymentReferenceCode
           //info product
           for (let indexe = 0; indexe < data[index].orderDetails.length; indexe++) {
               let pro = await product.findOne({_id:data[index].orderDetails[indexe].product})
               if(pro)
                dataOrders.orderDetails.push({code:pro.code,product:pro.name,prices:pro.prices,quantity:data[index].orderDetails[indexe].quantity})     
           }
           //info user
           let user = await auth.findOne({_id:data[index].buyer}) || await authec.findOne({_id:data[index].buyer})
           if(user)
            {
                if(user.sons)
                dataOrders.buyer.userType = 'rec'
                else 
                dataOrders.buyer.userType = 'public'

            dataOrders.buyer.name = user.name.first+' '+user.name.last    
            dataOrders.buyer.dni.dni = user.dni.dni
            dataOrders.buyer.dni.dniType = user.dni.dniType
            dataOrders.buyer.address =  user.address.country+', '+user.address.city+', '+user.address.addInfo
            dataOrders.buyer.mail = user.mail
            dataOrders.buyer.phone = user.phone 
            }

            if(dataOrders.orderDetails.length > 0 &&  dataOrders.buyer.userType != '')
                infoOrders.push(dataOrders)

            dataOrders = {
                _id : '',
                buyer:{
                 name:'',
                 address:'',
                 phone:'',
                 mail:'',
                 dni:{
                     dni:'',
                     dniType:''
                 },
                 userType:''
                },
                orderDetails:[],
                paymentRef:'',
                paymentRefCode:'',
                purchaseType:'',
                seller: 
                {
                    name: '',
                    id: '',
                    code: ''
                },                
                totalAmount:'',
                purchaseStatus:'',
                internalStatus:{
                    status:'',
                    deliveryDate:null,
                    receivedDate:null,
                    receiverName:''
                },
                payDate:null
     
             }
        }        
        return infoOrders

    }
    async changeStatusOrder(data)
    {
        let id = data.id
        let newStatus = data.internalStatus.status
        let dateDelivery = data.internalStatus.deliveryDate
        let data_ = await purchase.findOne({_id:id})
        data_.internalStatus.status = newStatus
        if(dateDelivery && newStatus == 'delivered')
            data_.internalStatus.deliveryDate = dateDelivery

        let result = await data_.save()
        return result       
    }

    // async updatePurchaseStatus(purchaseId, status) {
    //     let data = await purchase.findOne({_id: purchaseId});
    //     if(status == 'Approved' || status == 'Rejected' || status == 'Pending' || status == 'inBag') {
    //         data.purchaseStatus = status
    //         let res = await data.save()
    //         if(!('_id' in res)){}
    //         else {
    //             if(status == 'Approved') {
    //                 let prods = []
    //                 for (let index = 0; index < data.orderDetails.length; index++) {
    //                     let bone = {
    //                         product: data.orderDetails[index].product,
    //                         units: (data.orderDetails[index].quantity * -1)
    //                     }
    //                     prods.push(bone)
    //                 }
    //                 await Product.updateStock(prods)
    //             }
    //         }
    //         return res
    //     }
    //     return 'Unable to update purchase status'
    // }
}

let Purchase = new table();
module.exports= {Purchase};