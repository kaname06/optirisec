window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
// window.axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

window.$ = window.jQuery = require('jquery');

$('[data-toggle="tooltip"]').tooltip()

window.Vue = require('vue');
let cryptorjs = require('./Utilities/cryptor');
let swal = require('./Utilities/sweetAlert')
let jsmd5 = require('./Utilities/md5')

import VueRouter from 'vue-router';

Vue.use(VueRouter);

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)
//language
import lang from 'element-ui/lib/locale/lang/es'
import locale from 'element-ui/lib/locale'
const DataTables = require('vue-data-tables')

locale.use(lang)
Vue.use(DataTables)

import main from './Components/MainComponent.vue';
import dash from './Components/dashComponent.vue';
import login from './Components/LoginComponent.vue';
import products from './Components/ProductListComponent.vue';
import addproducts from './Components/AddProductComponent.vue';
import pd from './Components/ProductDetailComponent.vue';
import maincellar from './Components/MainCellarComponent.vue'; 
import ordersp from './Components/OrdersComponent.vue';
import sellers from './Components/SellersComponent.vue';
import resetPass from './Components/resetPassComponent.vue'

let paths = {
    main,
    login,
    products,
    dash,
    addproducts,
    pd,
    maincellar,
    sellers,
    ordersp,
    resetPass
}

let ec = [
    { path: '/', component: paths.main},
    { path: '/dash/:tabname?', component: paths.dash},
    {path: '/product-detail/:id?', component: paths.pd}
    // { path: '/products', component: paths.products},
    // { path: '/addproducts', component: paths.addproducts}
]
let cellar = [
    { path: '/products', component: paths.products},
    { path: '/addproducts', component: paths.addproducts},
    { path: '/', component: paths.maincellar},
    { path: '/purchases-orders', component: paths.ordersp},
    { path: '/sellers-view', component: paths.sellers}
]
  
// let auth = [{ path: '/login', component: paths.login}]
const ecomm = new VueRouter({
    uid: 'main',
    // mode: 'history',   
    routes: ec // short for `routes: routes`
})
const bod = new VueRouter({
    uid: 'manager',
    routes: cellar
})

Vue.component('main-component', require('./Components/MainComponent.vue').default);
Vue.component('login-component', require('./Components/LoginComponent.vue').default);
Vue.component('productlist-component', require('./Components/ProductListComponent.vue').default);
Vue.component('addproduct-component', require('./Components/AddProductComponent.vue').default);
Vue.component('maincellar-component',require('./Components/MainCellarComponent.vue').default);
Vue.component('navec-component',require('./Components/navEcComponent.vue').default);
Vue.component('register-component',require('./Components/RegisterComponent.vue').default);
Vue.component('resetpass-component',require('./Components/resetPassComponent.vue').default);

const mainauth = new Vue({
    el: '#login'
});
const mainapp = new Vue({
    el: '#main',
    router: ecomm
});
const mainprod = new Vue({
    el: '#products',
    router: bod
})
const repass = new Vue({
    el: '#resetp'
})




