//requiring and instancing const
const express = require('express');
const app = express();
const path = require('path')
const mongoose = require('mongoose')
const session = require('express-session')
const cParser = require('cookie-parser')
const updateJsonFile = require('update-json-file')
const fs = require('fs')
const cron = require('node-cron')
const qs = require('query-string')
const multer = require('multer')
//database connect
mongoose.connect('mongodb://127.0.0.1:27017/binary', { useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true})
.then(db => console.log("Database Connected Successfully"))
.catch(err => console.error("Houston, we have a problem here: \n"+err))


//settings
app.set('PORT', process.env.PORT || 3001);

//Middlewares
app.disable('x-powered-by');
app.use(express.json())
app.use(cParser())
app.use(session({
    key: 'user_session_id',
    secret: '10n1c0s3cr3tc0d3',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: (60*60*1000),
        httpOnly:true
    }
}))
// app.use(express.urlencoded({extended:true}))

app.get('/', (req, res) => {
    if (req.session.user && req.cookies.user_session_id) {
        if(req.session.user.userType == 'worker')
            res.redirect('/cpanel')
        else 
            res.sendFile(path.join(__dirname, '/public/index.html'));
    } else {
        res.sendFile(path.join(__dirname, '/public/index.html'));
    }
})
app.get('/cpanel', (req,res) => {
    if (req.session.user && req.cookies.user_session_id && req.session.user.userType == 'worker') {
       res.sendFile(path.join(__dirname, '/public/cellar.html'));
    } else {
        res.redirect('/');
    }
    // res.sendFile(path.join(__dirname, '/public/cellar.html'));
})

app.get('/logout', (req, res) => {
    let json = JSON.parse(fs.readFileSync('u23r20nl1n3.txt'));
    let user = '';
    if(req.session.user){
        user = req.session.user.dni.dni
    }
    for (const key in json) {
        if(json[key].dni == user) {
            // cookies.remove('room')
            updateJsonFile('u23r20nl1n3.txt', (dat) => {
                dat.splice(key, 1) 
                return dat;
            })
            break;
        }
    }
    req.session.user = null
    res.redirect('/auth/login');
})

//initializations
updateJsonFile('u23r20nl1n3.txt', (dat) => {
    dat = []
    return dat;
})

//CRON JOBS
//task to send mail with daily production orders
cron.schedule('59 4 * * *', async () => {
    let a = new Date()
    a.setHours(a.getHours() - 5)
    console.log(`(${a.getHours()}:${a.getMinutes()} at ${a.getDate()}/${a.getMonth() + 1}/${a.getFullYear()}) Running "Daily production orders" Scheduled task...`)
    
    let datis = await require('./src/Routes/purchase').getProductionOrders()
    require('./mailer').sendProductionOrders(datis)
});


//Routes
app.use('/auth', require('./src/Routes/auth').router)
app.use('/purchase', require('./src/Routes/purchase').router)
app.use('/product', require('./src/Routes/product').router)

//Public path
app.use(express.static(path.join(__dirname, '/public')));

//Server listen callback
app.listen(app.get('PORT'), () => {
    console.log(`Server running on port: ${app.get('PORT')}`)
})
