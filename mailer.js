const fs = require('fs')
const path = require('path')
//---------------Mailer
var nodemailer = require('nodemailer');
const user = JSON.parse(fs.readFileSync(path.join(__dirname, 'src/Utilities/keys.json'))).mailerData
// console.log(user)
// var user = {
//     name = process.env.SUPPORT_EMAIL_USER,
//     pass = process.env.SUPPORT_EMAIL_PASS
// }
var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: user.user,
    pass: user.password
  },
  tls: {
      rejectUnauthorized: false
  }
});

function sendProductionOrders(orderDetails) {
  var html = `<div style="    display: flex;
  justify-content: center;
  text-align: center;
  font-family: monospace;">
      <div style="padding: 25px 15px;
      box-shadow: 0px 12px 19px -8px rgba(0, 0, 0, 0.22), 0px 5px 23px -9px rgba(0, 0, 0, 0.07), 0px 4px 0px 0px rgba(0, 0, 0, 0);
      margin-top: 30px;
      border: dashed 2px #f5f5f5;
      border-radius: 5px;
      width: 600px;">
      <h1 style="    margin: 0px;
      text-transform: uppercase;
      color: #424141;font-weight: 600;"><strong>Ordenes de Productos IMPHA</strong></h1>`

      for (let index = 0; index < orderDetails.length; index++) {
        html+=`<h2 style="
          border-bottom: solid 1px #e8e8e8;
          padding-bottom: 10px;
          text-transform: uppercase;
          color: #424141;font-weight: 600;">(${index + 1}) Orden | ${orderDetails[index].buyReference}</h2>
          <p style="text-align: left;font-size: 13px;padding-left: 18px;"><strong style="text-transform: uppercase;letter-spacing: 1px;">Referencia de Pago:</strong> ${orderDetails[index].payReference}</p>
          <p style="text-align: left;font-size: 13px;padding-left: 18px;"><strong style="text-transform: uppercase;letter-spacing: 1px;">Comprador:</strong> ${orderDetails[index].buyer}</p>
          <p style="text-align: left;font-size: 13px;padding-left: 18px;"><strong style="text-transform: uppercase;letter-spacing: 1px;">Identificación:</strong> ${orderDetails[index].dni}</p>
          <p style="text-align: left;font-size: 13px;padding-left: 18px;"><strong style="text-transform: uppercase;letter-spacing: 1px;">E-Mail:</strong> ${orderDetails[index].mail}</p>
          <p style="text-align: left;font-size: 13px;padding-left: 18px;"><strong style="text-transform: uppercase;letter-spacing: 1px;">Dirección de envío:</strong> ${orderDetails[index].address}</p>
          <p style="text-align: left;font-size: 13px;padding-left: 18px;"><strong style="text-transform: uppercase;letter-spacing: 1px;">Teléfono de Contacto:</strong> ${orderDetails[index].phone || 'No Posee'}</p>                
          <table cellspacing="0" cellpadding="0" style="width: 95%;
          text-align: left;margin-top:15px;margin: 0 auto;">
              <thead>
                  <tr style="text-transform: uppercase;">
                      <th style="    padding: 13px 13px;
                      background: #000;
                      color: #fff;width: 140px;">Código de producto</th>
                      <th style="    padding: 13px 13px;
                      background: #000;
                      color: #fff;">Producto</th>
                      <th style="    padding: 13px 13px;
                      background: #424141;
                      color: #fff;width: 30px;text-align: end;">Cantidad</th>
                  </tr>
              </thead>
              <tbody>`

              for (let ind = 0; ind < orderDetails[index].orderDetails.length; ind++) {
                html+=`<tr>
                      <td style="padding: 10px 10px;
                      border-bottom: solid 1px #424141;font-weight: 600;background: #424141; color: #fff;">${orderDetails[index].orderDetails[ind].code}</td>
                      <td style="padding: 10px 10px;
                      border-bottom: solid 1px #cecece;">${orderDetails[index].orderDetails[ind].product}</td>
                      <td style="padding: 10px 10px;
                      border-bottom: solid 1px #cecece;text-align: center;">${orderDetails[index].orderDetails[ind].quantity}</td>
                  </tr>` 
              }

              html+=`</tbody>
              </table>`  
      }

      html+=`</div>
  </div>`
  
  let datete = new Date()
  datete.setHours(datete.getHours() - 5)

  var mailOptions = {
    from: user.name,
    to: 'impha.bodega@gmail.com, imphasas@gmail.com',
    subject: 'Ordenes de productos IMPHA - '+datete.getDate()+'/'+(datete.getMonth()+1)+'/'+datete.getFullYear(),
    html
  };

  if(orderDetails.length > 0) {
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.error('Mail error: ' + error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
  }
}

async function sendR(data) {
  var html = `<div style="    display: flex;
  justify-content: center;
  text-align: center;
  font-family: monospace;">
      <div style="padding: 25px 15px;
      box-shadow: 0px 12px 19px -8px rgba(0, 0, 0, 0.22), 0px 5px 23px -9px rgba(0, 0, 0, 0.07), 0px 4px 0px 0px rgba(0, 0, 0, 0);
      margin-top: 30px;
      border: dashed 2px #f5f5f5;
      border-radius: 5px;
      width: 600px;">
      <h1 style="    margin: 0px;
      text-transform: uppercase;
      color: #424141;font-weight: 600;"><strong>• Reclamo IMPHA •</strong></h1>`
      
      html+=`<h2 style="
      border-bottom: solid 1px #e8e8e8;
      padding-bottom: 10px;
      text-transform: uppercase;
      color: #424141;font-weight: 600;">Datos</h2>
      <p style="text-align: left;font-size: 13px;padding-left: 18px;"><strong style="text-transform: uppercase;letter-spacing: 1px;">Nombre:</strong> ${data.Rname}</p>
      <p style="text-align: left;font-size: 13px;padding-left: 18px;"><strong style="text-transform: uppercase;letter-spacing: 1px;">E-Mail:</strong> ${data.Rmail}</p>
      <p style="text-align: left;font-size: 13px;padding-left: 18px;"><strong style="text-transform: uppercase;letter-spacing: 1px;">Teléfono de Contacto:</strong> ${data.Rphone}</p>
      <p style="text-align: left;font-size: 13px;padding-left: 18px;"><strong style="text-transform: uppercase;letter-spacing: 1px;">Mensaje:</strong> ${data.Rmessage}</p>`                      
      
      html+=`</div>
  </div>`
  
  let datete = new Date()
  datete.setHours(datete.getHours() - 5)

  var mailOptions = {
    from: user.name,
    to: 'imphasas@gmail.com',
    // to: 'impha.bodega@gmail.com, imphasas@gmail.com',
    subject: 'Reclamo IMPHA - '+datete.getDate()+'/'+(datete.getMonth()+1)+'/'+datete.getFullYear(),
    html
  };
  let status = 'success'
  let a = await transporter.sendMail(mailOptions);
  return a;
}

const sendMailForgotPass = async (nickname, mail, token) => {
  if(!nickname || !mail)
      return {
          success: false,
          info: "missing data"
      }

      let html = `<div style="display:flex;align-items:center;justify-content:center;align-content:center;width:100%;color:#fff;">
      <div style="background:skyblue;padding:25px;border:solid 3px #fff;border-radius:5px;box-shadow:0 20px 30px 0 rgba(0,0,0,.10), 0 8px 20px 0 rgba(0,0,0,.14);">
        <h4 style="margin:0px 0px;font-weight:bold;letter-spacing:2px;">
          Recuperación de Contraseña
        </h4>
        <p style="padding:12px 10px;border-radius:5px;margin:12px 0px 0px 0px;background:#fff;color:#ababab;">
           Para recuperar la contraseña, por favor <br>      de click en el siguiente boton.   
        </p>
        <br>
          <a href="http://68.183.151.246:3001/auth/reset/password/${nickname}/${token}" style="padding:12px 15px;margin:15px 0px;color:#fff;cursor:pointer;text-decoration:none;background:#4aadd6;border-radius:5px;font-size:13px;">
           Recuperar 
          </a>
      </div>
    </div>`
      
      let datete = new Date()
      datete.setHours(datete.getHours() - 5)
    
      var mailOptions = {
        from: 'Servicio de Soporte OSORIO GROUP <'+user.user+'>',
        to: mail,
        subject: 'Restauración de contraseña TIENDA IMPHA - '+datete.getDate()+'/'+(datete.getMonth()+1)+'/'+datete.getFullYear(),
        html
      };

      let a = await transporter.sendMail(mailOptions);
      if(!a || !a.response || !a.response.includes('OK'))
          return {
              success: false,
              info: "An error ocurred while sending the mail"
          }
      return {
          success: true,
          info: "mail sended successfully"
      }
    
}


module.exports = {sendProductionOrders, sendR, sendMailForgotPass}
//---------------End Mailer