let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */
// mix.DisabledNotification();
mix.js(
[
    'public/js/jquery.js',
    // 'public/js/owl.carousel.min.js',
    'public/js/bootstrap.min.js',        
    'src/app.js'
], 
'public/js/app.js');
    
mix.styles(
[
    'public/css/bootstrap.min.css',
    'public/css/fonts.css',
    'public/css/animate.css',
    'public/css/owl.carousel.css',
    'public/css/slick.css',
    'public/css/slick-theme.css',
    'public/css/font-awesome.min.css',
    'public/css/mainn.css',
], 
'public/css/app.css');
